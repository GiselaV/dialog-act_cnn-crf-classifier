from train import *

sess=tf.Session()
#First let's load meta graph and restore weights
saver = tf.train.import_meta_graph('./results/25102018/checkpoints_swbd_2/best_validation.meta')
saver.restore(sess,tf.train.latest_checkpoint('./results/25102018/checkpoints_swbd_2/'))



# for op in tf.get_default_graph().get_operations():
#     print str(op.name)

graph = tf.get_default_graph()
FLAGS = initialize_flags()

global_step = graph.get_tensor_by_name("global_step:0")
input_x = graph.get_tensor_by_name("input_x:0")
input_y = graph.get_tensor_by_name("input_y:0")
input_speaker = graph.get_tensor_by_name("input_speaker:0")
dropout_keep_prob = graph.get_tensor_by_name("dropout_keep_prob:0")
context_lengths_t = graph.get_tensor_by_name("add_crf/Const:0")
unary_scores = graph.get_tensor_by_name("add_crf/Reshape:0")

correct_labels = 0
total_labels = 0
correct_sequences = 0
total_sequences = 0

seed = np.random.seed(12070185)
revs, word_idx_map, max_l, actDict, lexical_hyperparams = helpers.load_lexical_data(FLAGS.num_filters,
                                                                                    FLAGS.batch_size,
                                                                                    list(map(int, FLAGS.filter_sizes.split(","))),
                                                                                    FLAGS.num_filters,
                                                                                    FLAGS.context,
                                                                                    FLAGS.data_file)
W, this_batch_size, sent_len, filter_heights, num_filters, num_classes = lexical_hyperparams
voc_size, embedding_size = W.shape

# TODO: comment
datasets = helpers.createSetsWithSpeaker(revs, word_idx_map, batch_size=this_batch_size, max_l=max_l, filter_h=5, context=FLAGS.context)


lexical_set, labels, spkr_set = datasets
x_train, x_dev, x_test = lexical_set
y_train_, y_dev_, y_test_ = labels
train_spkrs, val_spkrs, test_spkrs = spkr_set

for batch in iterate_minibatches_no_shuffle(x_dev, y_dev_, val_spkrs, FLAGS.batch_size, FLAGS.speaker):
    #if FLAGS.speaker:
    inputs, targets, spkrs = batch
    feed_dict = {
        input_x: inputs,
        input_y: targets,
        input_speaker: spkrs,
        dropout_keep_prob: 1.0
    }

    scores, sequence_lengths = sess.run([unary_scores, context_lengths_t], feed_dict)
    log_likelihood, transition_params = tf.contrib.crf.crf_log_likelihood(unary_scores, input_y, context_lengths_t)
    for scores_, targets_, sequence_length_ in zip(scores, targets, sequence_lengths):
        scores = scores_[:sequence_length_]
        y_ = targets_[:sequence_length_]

        viterbi_sequence, _ = tf.contrib.crf.viterbi_decode(unary_scores, transition_params)
        correct_labels += np.sum(np.equal(viterbi_sequence[-1], y_[-1]))

        total_labels += 1#sequence_length_
        correct_sequences += np.sum(np.equal(viterbi_sequence, y_))
        total_sequences += sequence_length_

    accuracy = 100.0 * correct_labels / float(total_labels)
    sequence_accuracy = 100.0 * correct_sequences / float(total_sequences)



#for n in tf.get_default_graph().as_graph_def().node:
 #   print n.name


"""
gradients/add_crf/cond/rnn/while/ExpandDims_grad/Shape
gradients/add_crf/cond/rnn/while/ExpandDims_grad/Reshape
gradients/add_crf/cond/rnn/while/add_1/Enter_grad/b_acc
gradients/add_crf/cond/rnn/while/add_1/Enter_grad/b_acc_1
gradients/add_crf/cond/rnn/while/add_1/Enter_grad/b_acc_2
gradients/add_crf/cond/rnn/while/add_1/Enter_grad/Switch
gradients/add_crf/cond/rnn/while/add_1/Enter_grad/Add
gradients/add_crf/cond/rnn/while/add_1/Enter_grad/NextIteration
gradients/add_crf/cond/rnn/while/add_1/Enter_grad/b_acc_3
"""