#! /usr/bin/env python
import sys
import tensorflow as tf
import numpy as np
import os
import time
import datetime
import data_utils as helpers
from model import TextCNN
from tensorflow.contrib import learn


os.environ["CUDA_VISIBLE_DEVICES"] = sys.argv[1]
total_iterations = 0
# Best validation accuracy seen so far.
best_validation_accuracy = 0.0

# Iteration-number for last improvement to validation accuracy.
last_improvement = 0

# Stop optimization if no improvement found in this many iterations.
require_improvement = 5

# Parameters
# ==================================================
def initialize_flags():
    # Data loading params
    start = time.time()
    corpus = "ICSI"#""SWBD"
    base_folder = "/mount/arbeitsdaten34/projekte/slu/Daniel/projects/DA_data/"
    lexical_file = base_folder + corpus + "/lexical/w2v_lexical_data_with_speaker_aut_transcripts_context_12.p"
    # base_folder = "/mount/arbeitsdaten34/projekte/slu/Daniel/projects/DA_data/"
    # lexical_file = base_folder+corpus+"/lexical/w2v_lexical_data_with_speaker_aut_transcripts_context_12.p"

    acoustic_file = base_folder+corpus+"/acoustic/mfcc_acoustic_data_context_12.msgpack"

    tf.flags.DEFINE_string("data_file", lexical_file, "Data source")
    tf.flags.DEFINE_string("data_ac_file", acoustic_file, "Data source")

    tf.flags.DEFINE_string("corpus", corpus, "name of the corpus, swbd or icsi")
    tf.flags.DEFINE_integer("context", 2, "Number of sentences in the context(default: 1)")
    tf.flags.DEFINE_boolean("crf", True, "take cnn or crf")
    tf.flags.DEFINE_boolean("speaker", True, "take speaker or not")

    # Model Hyperparameters
    tf.flags.DEFINE_string("filter_sizes", "3,4,5", "Comma-separated filter sizes (default: '3,4,5')")
    tf.flags.DEFINE_integer("num_filters", 100, "Number of filters per filter size (default: 128)")
    tf.flags.DEFINE_float("dropout_keep_prob", 0.5, "Dropout keep probability (default: 0.5)")
    tf.flags.DEFINE_float("l2_reg_lambda", 0.0, "L2 regularization lambda (default: 0.0)")
    tf.flags.DEFINE_float("learning_rate", 1e-1, "Size of the learning rate(default: 1e-3)")

    # Training parameters
    tf.flags.DEFINE_integer("batch_size", 70, "Batch Size (default: 150)")
    tf.flags.DEFINE_integer("num_epochs", 30, "Number of training epochs (default: 200)")
    tf.flags.DEFINE_integer("evaluate_every", 100, "Evaluate model on dev set after this many steps (default: 100)")
    tf.flags.DEFINE_integer("checkpoint_every", 10000, "Save model after this many steps (default: 100)")
    tf.flags.DEFINE_integer("num_checkpoints", 5, "Number of checkpoints to store (default: 5)")


    FLAGS = tf.flags.FLAGS
    FLAGS.is_parsed()
    print("\nParameters:")
    for attr, value in sorted(FLAGS.__flags.items()):
        print("{}={}".format(attr.upper(), value.value))
    print("")


    # Data Preparation
    # ==================================================

    # Load data
    return FLAGS

def build_model(FLAGS, optimizer, learning_rate):
    print("Loading data...")
    seed = np.random.seed(12070185)
    revs, word_idx_map, max_l, actDict, lexical_hyperparams = helpers.load_lexical_data(FLAGS.num_filters,
                                                                                        FLAGS.batch_size,
                                                                                        list(map(int, FLAGS.filter_sizes.split(","))),
                                                                                        FLAGS.num_filters,
                                                                                        FLAGS.context,
                                                                                        FLAGS.data_file)
    W, this_batch_size, sent_len, filter_heights, num_filters, num_classes = lexical_hyperparams
    voc_size, embedding_size = W.shape

    # TODO: comment
    # datasets = helpers.createSetsWithSpeaker(revs, word_idx_map, batch_size=this_batch_size, max_l=max_l, filter_h=5, context=FLAGS.context)
    datasets = helpers.createSetsWithSpeaker_with_AT(revs, word_idx_map, batch_size=this_batch_size, max_l=max_l,
                                                     filter_h=5, context=FLAGS.context, model="lexical")
    # datasets = helpers.createSetsForPrediction(revs, word_idx_map, batch_size=this_batch_size, max_l=max_l, filter_h=5, context=FLAGS.context)


    # Training
    # ==================================================
    # filter_sizes =list(map(int, FLAGS.filter_sizes.split(","))
    #
    # def build_model(sent_len, num_classes, voc_size, embedding_size, filter_sizes, num_filters, context, crf, seed, l2_reg_lambda,
    #                 batch_size)
    print "Optimizer:" + str(optimizer)
    print "Learning Rate: " + str(learning_rate)
    print  "Context: " + str(FLAGS.context)
    #with tf.Graph().as_default():
    tf.set_random_seed(seed)
    sess = tf.Session()
    with sess.as_default():

        cnn = TextCNN(
            sequence_length=sent_len,
            num_classes=num_classes,
            vocab_size=voc_size,
            embedding_size=embedding_size,
            filter_sizes=list(map(int, FLAGS.filter_sizes.split(","))),
            num_filters=FLAGS.num_filters,
            context=FLAGS.context,
            crf=FLAGS.crf,
            speaker=FLAGS.speaker,
            seed=seed,
            l2_reg_lambda=FLAGS.l2_reg_lambda,
            batch_size=FLAGS.batch_size)

        # cnn.add_cnn(voc_size, FLAGS.embedding_dim, list(map(int, FLAGS.filter_sizes.split(","))),
        #                 FLAGS.num_filters, x_train.shape[1])

        # Define Training procedure
        global_step = tf.Variable(0, name="global_step", trainable=False)
       #  learning_rate = tf.train.exponential_decay(starter_learning_rate, global_step, 2000, 0.10, staircase=True)

        # train_op = tf.train.MomentumOptimizer(FLAGS.learning_rate, 1e-2).minimize(cnn.loss, global_step=global_step)
        # train_op = optimizer(learning_rate, 3e-3).minimize(cnn.loss, global_step=global_step)
        train_op = optimizer(learning_rate).minimize(cnn.loss, global_step=global_step)
        # train_op = tf.train.AdadeltaOptimizer(1e-3).minimize(cnn.loss, global_step=global_step)


        # Initialize all variables
        sess.run(tf.global_variables_initializer())
        sess.run(cnn.W.assign(W))
    return train_op, datasets, cnn, sess, global_step, num_classes, this_batch_size


def iterate_minibatches(inputs, targets, speakers, batch_size, speaker):

    assert len(inputs) == len(targets) == len(speakers)
    inputs = np.asarray(inputs)
    targets = np.asarray(targets)
    indices = np.array(range(len(inputs)))
    np.random.shuffle(indices)

    for start_idx in range(0, len(indices) - batch_size + 1, batch_size):
        excerpt = indices[start_idx:start_idx + batch_size]
        #excerpt = slice(start_idx, start_idx + batch_size)
        if speaker:
            yield inputs[excerpt], targets[excerpt], speakers[excerpt]
        else:
            yield inputs[excerpt], targets[excerpt]
            
def iterate_minibatches_no_shuffle(inputs, targets, speakers, batch_size, speaker):

    assert len(inputs) == len(targets) == len(speakers)
    inputs = np.asarray(inputs)
    targets = np.asarray(targets)
    indices = np.array(range(len(inputs)))

    for start_idx in range(0, len(indices) - batch_size + 1, batch_size):
        excerpt = indices[start_idx:start_idx + batch_size]
        #excerpt = slice(start_idx, start_idx + batch_size)
        if speaker:
            yield inputs[excerpt], targets[excerpt], speakers[excerpt]
        else:
            yield inputs[excerpt], targets[excerpt]


def evaluate_cnn(x_set, y_set, cnn, sess, global_step, batch_size):
    sum_predictions = 0.0
    for batch in iterate_minibatches(x_set, y_set, batch_size):
        inputs, targets, speakers = batch
        feed_dict = {
            cnn.input_x: inputs,
            cnn.input_y: targets,
            cnn.dropout_keep_prob: 1.0
        }

        step, predictions = sess.run([global_step, cnn.correct_predictions], feed_dict)
        sum_predictions += np.sum(predictions.astype(float))

    accuracy = 100.0 * sum_predictions / len(y_set)
    return accuracy

def evaluate_crf(x_set, y_set, spkrs_set, cnn, sess, FLAGS, test_crf):
    correct_labels = 0
    total_labels = 0
    correct_sequences = 0
    total_sequences = 0
    for batch in iterate_minibatches_no_shuffle(x_set, y_set, spkrs_set, FLAGS.batch_size, FLAGS.speaker):
        if FLAGS.speaker:
            inputs, targets, spkrs = batch
            feed_dict = {
                cnn.input_x: inputs,
                cnn.input_y: targets,
                cnn.input_speaker: spkrs,
                cnn.dropout_keep_prob: 1.0
            }
        else:
            inputs, targets = batch
            feed_dict = {
                cnn.input_x: inputs,
                cnn.input_y: targets,
                cnn.dropout_keep_prob: 1.0
            }

        scores, transition_params, sequence_lengths = sess.run([cnn.unary_scores,
                                                                cnn.transition_params,
                                                                cnn.context_lengths_t],
                                                               feed_dict)

        for scores_, targets_, sequence_length_ in zip(scores, targets, sequence_lengths):
            scores = scores_[:sequence_length_]
            y_ = targets_[:sequence_length_]
            viterbi_sequence, _ = tf.contrib.crf.viterbi_decode(scores, transition_params)
            correct_labels += np.sum(np.equal(viterbi_sequence[-1], y_[-1]))
            if test_crf:
                print viterbi_sequence, y_
            total_labels += 1#sequence_length_
            correct_sequences += np.sum(np.equal(viterbi_sequence, y_))
            total_sequences += sequence_length_

    accuracy = 100.0 * correct_labels / float(total_labels)
    sequence_accuracy = 100.0 * correct_sequences / float(total_sequences)
    return accuracy, sequence_accuracy


def run_model(FLAGS, train_op, datasets, cnn, sess, global_step, num_classes, save_dir, num_epochs=10, batch_size=50):
    lexical_set, labels, spkr_set, acData, acoustic_embedding, ac_shape, lexData_e, lexData_k = datasets
    x_train, x_dev, x_test = lexical_set
    x_train_e, x_dev_e, x_test_e = lexData_e
    x_train_k, x_dev_k, x_test_k = lexData_k
    y_train_, y_dev_, y_test_ = labels
    train_spkrs, val_spkrs, test_spkrs = spkr_set


    saver = tf.train.Saver()
    #save_dir = './results/27102018/run%d/%s_checkpoints_%d/' % (run, FLAGS.corpus, FLAGS.context)

    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    save_path = os.path.join(save_dir, 'best_validation')

    if FLAGS.crf:
        y_train = y_train_
        y_dev = y_dev_
        y_test = y_test_
    else:
        y_train = helpers.convert_to_one_hot(y_train_[:, -1], num_classes)
        y_dev = helpers.convert_to_one_hot(y_dev_[:, -1], num_classes)
        y_test = helpers.convert_to_one_hot(y_test_[:, -1], num_classes)

    denominator = len(x_train)/batch_size

    # Ensure we update the global variables rather than local copies.
    global total_iterations
    global best_validation_accuracy
    global last_improvement

    for epoch in range(num_epochs):

        #print "Epoch: ",  epoch+1
        #print "Dev:\tTest:\tLoss:"
        loss = 0.0
        for batch in iterate_minibatches(x_train, y_train, train_spkrs, FLAGS.batch_size, FLAGS.speaker):
            if FLAGS.speaker:
                inputs, targets, spkrs = batch
                feed_dict_train = {
                    cnn.input_x: inputs,
                    cnn.input_y: targets,
                    cnn.input_speaker: spkrs,
                    cnn.dropout_keep_prob: FLAGS.dropout_keep_prob
                }
            else:
                inputs, targets = batch
                feed_dict_train = {
                    cnn.input_x: inputs,
                    cnn.input_y: targets,
                    cnn.dropout_keep_prob: FLAGS.dropout_keep_prob
                }
            


            if FLAGS.crf:
                _, step, batch_loss = sess.run([train_op, global_step, cnn.loss], feed_dict_train)
                loss += batch_loss
            else:
                _, step, batch_loss = sess.run([train_op, global_step, cnn.loss], feed_dict_train)
                loss += batch_loss

        loss = loss / denominator
        #train_accuracy = evaluate(x_train, y_train)
        print_dev = False
        print_test = False
        if FLAGS.crf:
            dev_accuracy, dseq_accuracy = evaluate_crf(x_dev, y_dev, val_spkrs, cnn, sess, FLAGS, print_dev)
            test_accuracy, tseq_accuracy = evaluate_crf(x_test, y_test, test_spkrs, cnn, sess, FLAGS, print_test)
            # dev_accuracy_e, dseq_accuracy_e = evaluate_crf(x_dev_e, y_dev, val_spkrs, cnn, sess, FLAGS, print_dev)
            # test_accuracy_e, tseq_accuracy_e = evaluate_crf(x_test_e, y_test, test_spkrs, cnn, sess, FLAGS, print_test)
            # dev_accuracy_k, dseq_accuracy_k = evaluate_crf(x_dev_k, y_dev, val_spkrs, cnn, sess, FLAGS, print_dev)
            # test_accuracy_k, tseq_accuracy_k = evaluate_crf(x_test_k, y_test, test_spkrs, cnn, sess, FLAGS, print_test)


            # If validation accuracy is an improvement over best-known.
            if dev_accuracy > best_validation_accuracy:
                # Update the best-known validation accuracy.
                best_validation_accuracy = dev_accuracy

                # Set the iteration for the last improvement to current.
                last_improvement = total_iterations

                # Save all variables of the TensorFlow graph to file.
                saver.save(sess=sess, save_path=save_path)

                # A string to be printed below, shows improvement found.
                improved_str = '*'


            else:
                # An empty string to be printed below.
                # Shows that no improvement was found.
                improved_str = ''
            print_epoch = epoch +1
            print "%.2f" % dev_accuracy, "\t%.2f" % test_accuracy, "Epoch %d" % print_epoch, \
                "\tloss %.2f" % loss, "%s" %improved_str
            # print "%.2f" % dev_accuracy_e, "\t%.2f" % test_accuracy_e, "Epoch e" , \
            #     "\tloss %.2f" % loss, "%s" %improved_str
            # print "%.2f" % dev_accuracy_k, "\t%.2f" % test_accuracy_k, "Epoch k" , \
              #  "\tloss %.2f" % loss, "%s" %improved_str
            
        else:
            dev_accuracy = evaluate_cnn(x_dev, y_dev, cnn, sess, global_step, FLAGS)
            test_accuracy = evaluate_cnn(x_test, y_test, cnn, sess, global_step, FLAGS)
            print "%.2f" % dev_accuracy, "%.2f" % test_accuracy, "%.2f" % loss


    tf.reset_default_graph()
    total_iterations = 0

    # Best validation accuracy seen so far.
    best_validation_accuracy = 0.0

    # Iteration-number for last improvement to validation accuracy.
    last_improvement = 0

    # Stop optimization if no improvement found in this many iterations.
    require_improvement = 5

#start = time.time()
#FLAGS = initialize_flags()
#optimizer = tf.train.GradientDescentOptimizer
#train_op, datasets, cnn, sess, global_step, num_classes, batch_size = build_model(FLAGS, optimizer, FLAGS.learning_rate)
#run_model(FLAGS, train_op, datasets, cnn, sess, global_step, num_classes, FLAGS.num_epochs, batch_size=batch_size)
#end = time.time()
#print "\n\nThe time elapsed: ", end-start, "seconds"
