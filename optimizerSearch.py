
import train
import tensorflow as tf
import time
import sys


def check_optimizer():
    tf_optimizers = {"GDO": tf.train.GradientDescentOptimizer,
                     "AdaDelta": tf.train.AdadeltaOptimizer,
                     "Adagrad": tf.train.AdagradOptimizer,
                     "AdagradDA": tf.train.AdagradDAOptimizer,
                     "RMSProp": tf.train.RMSPropOptimizer,
                     "Adam": tf.train.AdamOptimizer,
                     "Momentum": tf.train.MomentumOptimizer}
    rates = [1e-3, 2e-3, 3e-3, 4e-3, 5e-3, 6e-3, 7e-3, 8e-3, 9e-3, 1e-2, 2e-2, 3e-2, 4e-2, 5e-2, 6e-2, 7e-2, 8e-2, 9e-2,
             1e-1, 2e-1, 3e-1, 4e-1, 5e-1, 6e-1, 7e-1, 8e-1, 9e-1]
    # rates = [1e-4, 2e-4, 3e-4, 4e-4, 5e-4, 6e-4, 7e-4, 8e-4, 9e-4]
    FLAGS = train.initialize_flags()
    opt = sys.argv[2]
    for learning_rate in rates:
        start = time.time()
        optimizer = tf_optimizers[opt]
        train_op, datasets, cnn, sess, global_step, num_classes, batch_size = train.build_model(FLAGS,
                                                                                                optimizer,
                                                                                                learning_rate)
        train.run_model(FLAGS, train_op, datasets, cnn, sess, global_step, num_classes, 30, 70)
        end = time.time()
        print "\n\nThe time elapsed: ", end-start, "seconds"


def check_context():
    tf_optimizers = {"GDO": tf.train.GradientDescentOptimizer,
                     "AdaDelta": tf.train.AdadeltaOptimizer,
                     "Adagrad": tf.train.AdagradOptimizer,
                     "AdagradDA": tf.train.AdagradDAOptimizer,
                     "RMSProp": tf.train.RMSPropOptimizer,
                     "Adam": tf.train.AdamOptimizer,
                     "Momentum": tf.train.MomentumOptimizer}
    rates = [2e-3, 3e-3, 4e-3, 5e-3, 6e-3, 7e-3, 8e-3, 9e-3, 1e-2, 2e-2, 3e-2, 4e-2, 5e-2, 6e-2, 7e-2, 8e-2, 9e-2, 1e-1,
             2e-1, 3e-1, 4e-1, 5e-1, 6e-1, 7e-1, 8e-1, 9e-1]
    contexts = [2, 3, 4, 5, 6]
    FLAGS = train.initialize_flags()
    opt = sys.argv[2]
    learning_rate = float(sys.argv[3])
    for context in contexts:
        start = time.time()
        optimizer = tf_optimizers[opt]
        FLAGS.context = context
        train_op, datasets, cnn, sess, global_step, num_classes, batch_size = train.build_model(FLAGS,
                                                                                                optimizer,
                                                                                                learning_rate)
        train.run_model(FLAGS, train_op, datasets, cnn, sess, global_step, num_classes, FLAGS.num_epochs, batch_size)
        end = time.time()
        print "\n\nThe time elapsed: ", end-start, "seconds"


def check_lr_optimizer():
        tf_optimizers = {"GDO": tf.train.GradientDescentOptimizer,
                         "AdaDelta": tf.train.AdadeltaOptimizer,
                         "Adagrad": tf.train.AdagradOptimizer,
                         "AdagradDA": tf.train.AdagradDAOptimizer,
                         "RMSProp": tf.train.RMSPropOptimizer,
                         "Adam": tf.train.AdamOptimizer,
                         "Momentum": tf.train.MomentumOptimizer}

        learning_rate = 1e-3
        FLAGS = train.initialize_flags()
        optimizers = ["GDO", "AdaDelta", "Adagrad", "Adam"]
        for opt in optimizers:
            start = time.time()
            optimizer = tf_optimizers[opt]
            train_op, datasets, cnn, sess, global_step, num_classes, batch_size = train.build_model(FLAGS,
                                                                                                    optimizer,
                                                                                                    learning_rate)
            train.run_model(FLAGS, train_op, datasets, cnn, sess, global_step, num_classes, 30, 170)
            end = time.time()
            print "\n\nThe time elapsed: ", end-start, "seconds"


def run_average():
    tf_optimizers = {"GDO": tf.train.GradientDescentOptimizer,
                     "AdaDelta": tf.train.AdadeltaOptimizer,
                     "Adagrad": tf.train.AdagradOptimizer,
                     "AdagradDA": tf.train.AdagradDAOptimizer,
                     "RMSProp": tf.train.RMSPropOptimizer,
                     "Adam": tf.train.AdamOptimizer,
                     "Momentum": tf.train.MomentumOptimizer}

    FLAGS = train.initialize_flags()
    opt = sys.argv[2]
    learning_rate = float(sys.argv[3])
    for i in range(1, 6):
        for context in range(2, 12):
            start = time.time()
            optimizer = tf_optimizers[opt]
            FLAGS.context = context
            train_op, datasets, cnn, sess, global_step, num_classes, batch_size = train.build_model(FLAGS,
                                                                                                    optimizer,
                                                                                                    learning_rate)
            run = i
            save_dir = "./results/28102018MT_NOPUNC/run%d/%s_checkpoints_%d/" % (run, FLAGS.corpus, context)
            # print save_dir
            train.run_model(FLAGS, train_op, datasets, cnn, sess, global_step, num_classes, save_dir, FLAGS.num_epochs, batch_size)
            end = time.time()
            print "\n\nThe time elapsed: ", end-start, "seconds"


# check_optimizer()
# check_context()
# check_lr_optimizer()
run_average()
