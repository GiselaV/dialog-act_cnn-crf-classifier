import tensorflow as tf
import numpy as np
import cPickle
import random
import string


needed_samples_train = 0
needed_samples_test = 0
needed_samples_dev = 0

def get_idx_from_sent(sents, word_idx_map, max_l, filter_h=5):
    """
    Transforms sentence into a list of indices. Pad with zeroes.
    """
    x = []
    pad = filter_h - 1
    for i in xrange(pad):
        x.append(0)

    for sent in sents:
        temp_x = []
        if sent != "dummy_text":
            # This line removes punctuation
            sent = sent.translate(string.maketrans("",""), string.punctuation)
            words = sent.split()
            for word in words:
                if word in word_idx_map:
                    temp_x.append(word_idx_map[word])
        while len(temp_x) < max_l:
            temp_x.append(0)
        x += temp_x
    for i in xrange(pad):
        x.append(0)
    return x


def createSetsWithSpeaker(revs, word_idx_map, max_l, filter_h=5, context=1, batch_size=150):
    lexTrain, lexTest, lexVal = [], [], []
    train_labels, test_labels, val_labels = [], [], []
    train_spkrs, test_spkrs, val_spkrs = [], [], []
    ac_train, ac_test, ac_val = [], [], []
    notFound = 0

    for rev in revs:
        context_2reverse = rev["text"][:context]
        context_2reverse.reverse()
        y_2reverse = rev["ys"][:context]
        y_2reverse.reverse()
        spkrs_2reverse = rev["spkrs"][:context]
        spkrs_2reverse.reverse()

        sent = get_idx_from_sent(context_2reverse, word_idx_map, max_l, filter_h)

        spkrs = []

        for x in spkrs_2reverse:
            spkrs += [1] if x == spkrs_2reverse[-1] else [0]


        if rev["split"] == "test":
            lexTest.append(sent)
            test_labels.append(y_2reverse)
            test_spkrs.append(spkrs)
        elif rev["split"] == "train":
            lexTrain.append(sent)
            train_labels.append(y_2reverse)
            train_spkrs.append(spkrs)
        elif rev["split"] == "valid":
            lexVal.append(sent)
            val_labels.append(y_2reverse)
            val_spkrs.append(spkrs)

    # loop
    lexTrain, train_labels, train_spkrs, ac_train, needed_samples_train = batch_size_modulo(batch_size, lexTrain, ac_train,train_labels, train_spkrs)
    lexVal, val_labels, val_spkrs, ac_val,needed_samples_dev = batch_size_modulo(batch_size, lexVal, ac_val, val_labels, val_spkrs)
    lexTest, test_labels, test_spkrs, ac_test, needed_samples_test = batch_size_modulo(batch_size, lexTest, ac_test, test_labels, test_spkrs)


    # data = open("/mount/arbeitsdaten31/studenten1/vallejga/Thesis/repository/Logs/icsi/icsi_train_gisela", "w")
    # for rev in lexTrain:
    #     data.write(str(rev) + "\n")
    # data.close()

    lexTrain = np.array(lexTrain, dtype="int")
    lexTest = np.array(lexTest, dtype="int")
    lexVal = np.array(lexVal, dtype="int")

    train_labels = np.array(train_labels, "int32")+1
    test_labels = np.array(test_labels, "int32")+1
    val_labels = np.array(val_labels, "int32")+1

    train_spkrs = np.array(train_spkrs, "int32")
    val_spkrs = np.array(val_spkrs, "int32")
    test_spkrs = np.array(test_spkrs, "int32")

    lexData = [lexTrain, lexVal, lexTest]
    labels = [train_labels, val_labels, test_labels]
    spkrData = [train_spkrs, val_spkrs, test_spkrs]


    return lexData, labels, spkrData


def createSetsWithSpeaker_with_AT(revs, word_idx_map, max_l, filter_h=5, context=1, batch_size=150, model='acoustic',
                                  acousData=[]):
    lexTrain, lexTest, lexVal = [], [], []
    lexTrain_e, lexTest_e, lexVal_e = [], [], []
    lexTrain_k, lexTest_k, lexVal_k = [], [], []

    train_labels, test_labels, val_labels = [], [], []
    train_spkrs, test_spkrs, val_spkrs = [], [], []
    notFound = 0

    print "number of utts: ", len(revs)

    ''' START NEW CODE'''
    acTrain, acTest, acVal = [], [], []
    acoustic_embedding = []
    models = ["lexical", "acoustic", "lex_ac"]
    acoustic = False
    if model in models[1:]:
        acoustic = True
        revs = [rev for rev in revs if rev["code"] in acousData]
        shape = acousData[revs[0]["code"]].shape  # shape = (1, 1048, 13)
        print("Ac_Data Shape:" + str(shape))
        acoustic_embedding = np.zeros((len(revs) + 1, shape[1], shape[2]), float)
        num_frames = shape[1]
        print "number of utterances", len(revs)
    ''' FINISH NEW CODE'''

    all_classes = set()

    for i, rev in enumerate(revs):
        context_2reverse = rev["text"][:context]
        context_2reverse.reverse()

        context_2reverse_e = rev["text_e"][:context]
        context_2reverse_e.reverse()

        context_2reverse_k = rev["text_k"][:context]
        context_2reverse_k.reverse()

        y_2reverse = rev["ys"][:context]
        y_2reverse.reverse()

        spkrs_2reverse = rev["spkrs"][:context]
        spkrs_2reverse.reverse()

        all_classes.add(rev["y"])

        ''' START NEW CODE'''
        if acoustic:
            ac_idxs = [0] * context
            codes_not_reversed = rev["codes"][:context]
            current_code = rev["code"]
            acoustic_embedding[i + 1, :, :] = acousData[rev["code"]][0, :, :]
            for j, code, in enumerate(codes_not_reversed):
                if code != 'padding_code':
                    ac_idxs[j] = i + 1 - j  # This assigns the index from the embedding matrix
            ac_idxs.reverse()
        ''' FINISH NEW CODE'''

        sent = get_idx_from_sent(context_2reverse, word_idx_map, max_l, filter_h)

        sent_e = get_idx_from_sent(context_2reverse_e, word_idx_map, max_l, filter_h)

        sent_k = get_idx_from_sent(context_2reverse_k, word_idx_map, max_l, filter_h)

        spkrs = []

        for x in spkrs_2reverse:
            spkrs += [1] if x == spkrs_2reverse[-1] else [0]

        if rev["split"] == "test":
            lexTest.append(sent)
            lexTest_e.append(sent_e)
            lexTest_k.append(sent_k)

            test_labels.append(y_2reverse)
            test_spkrs.append(spkrs)

            ''' START NEW CODE'''
            if acoustic:
                acTest.append(ac_idxs)
            ''' FINISH NEW CODE'''

        elif rev["split"] == "train":
            lexTrain.append(sent)
            lexTrain_e.append(sent_e)
            lexTrain_k.append(sent_k)

            train_labels.append(y_2reverse)
            train_spkrs.append(spkrs)

            ''' START NEW CODE'''
            if acoustic:
                acTrain.append(ac_idxs)
            ''' FINISH NEW CODE'''

        elif rev["split"] == "valid":
            lexVal.append(sent)
            lexVal_e.append(sent_e)
            lexVal_k.append(sent_k)

            val_labels.append(y_2reverse)
            val_spkrs.append(spkrs)

            ''' START NEW CODE'''
            if acoustic:
                acVal.append(ac_idxs)
            ''' FINISH NEW CODE'''

    # print "All classes", all_classes, len(all_classes)
    # raw_input("WAIT")

    global needed_samples_dev
    global needed_samples_test
    global needed_samples_train

    # loop
    # Train
    lexTrain, acTrain, train_labels, train_spkrs, needed_samples_train = batch_size_modulo(batch_size, lexTrain,
                                                                                           acTrain, train_labels,
                                                                                           train_spkrs, acoustic)

    lexTrain_e, acTrain, train_labels, train_spkrs, needed_samples_train = batch_size_modulo(batch_size, lexTrain_e,
                                                                                             acTrain, train_labels,
                                                                                             train_spkrs, acoustic)

    lexTrain_k, acTrain, train_labels, train_spkrs, needed_samples_train = batch_size_modulo(batch_size, lexTrain_k,
                                                                                             acTrain, train_labels,
                                                                                             train_spkrs, acoustic)


    # Validation
    lexVal, acVal, val_labels, val_spkrs, needed_samples_dev = batch_size_modulo(batch_size, lexVal, acVal, val_labels,
                                                                                 val_spkrs, acoustic)

    lexVal_e, acVal, val_labels, val_spkrs, needed_samples_dev = batch_size_modulo(batch_size, lexVal_e, acVal, val_labels,
                                                                                   val_spkrs, acoustic)

    lexVal_k, acVal, val_labels, val_spkrs, needed_samples_dev = batch_size_modulo(batch_size, lexVal_k, acVal, val_labels,
                                                                                   val_spkrs, acoustic)


    # Test
    lexTest, acTest, test_labels, test_spkrs, needed_samples_test = batch_size_modulo(batch_size, lexTest, acTest,
                                                                                      test_labels, test_spkrs, acoustic)

    lexTest_e, acTest, test_labels, test_spkrs, needed_samples_test = batch_size_modulo(batch_size, lexTest_e, acTest,
                                                                                        test_labels, test_spkrs, acoustic)

    lexTest_k, acTest, test_labels, test_spkrs, needed_samples_test = batch_size_modulo(batch_size, lexTest_k, acTest,
                                                                                        test_labels, test_spkrs, acoustic)

    # data = open("/mount/arbeitsdaten31/studenten1/vallejga/Thesis/repository/Logs/icsi/icsi_train_gisela", "w")
    # for rev in lexTrain:
    #     data.write(str(rev) + "\n")
    # data.close()

    lexTrain = np.array(lexTrain, dtype="int")
    lexTest = np.array(lexTest, dtype="int")
    lexVal = np.array(lexVal, dtype="int")

    lexTrain_e = np.array(lexTrain_e, dtype="int")
    lexTest_e = np.array(lexTest_e, dtype="int")
    lexVal_e = np.array(lexVal_e, dtype="int")

    lexTrain_k = np.array(lexTrain_k, dtype="int")
    lexTest_k = np.array(lexTest_k, dtype="int")
    lexVal_k = np.array(lexVal_k, dtype="int")

    acTrain = np.array(acTrain, dtype="int")
    acTest = np.array(acTest, dtype="int")
    acVal = np.array(acVal, dtype="int")

    train_labels = np.array(train_labels, "int32") + 1
    test_labels = np.array(test_labels, "int32") + 1
    val_labels = np.array(val_labels, "int32") + 1

    train_spkrs = np.array(train_spkrs, "int32")
    val_spkrs = np.array(val_spkrs, "int32")
    test_spkrs = np.array(test_spkrs, "int32")

    lexData = [lexTrain, lexVal, lexTest]
    print "lexData", [lexTrain.shape, lexVal.shape, lexTest.shape]
    lexData_e = [lexTrain_e, lexVal_e, lexTest_e]
    print "lexData_e", [lexTrain_e.shape, lexVal_e.shape, lexTest_e.shape]
    lexData_k = [lexTrain_k, lexVal_k, lexTest_k]
    print "lexData_k", [lexTrain_k.shape, lexVal_k.shape, lexTest_k.shape]

    acData = [acTrain, acVal, acTest]
    print "acData", [acTrain.shape, acVal.shape, acTest.shape]
    labels = [train_labels, val_labels, test_labels]
    print "labels", [train_labels.shape, val_labels.shape, test_labels.shape]
    spkrData = [train_spkrs, val_spkrs, test_spkrs]
    print "spkrData", [train_spkrs.shape, val_spkrs.shape, test_spkrs.shape]
    # ac_shape = acoustic_embedding.shape
    ac_shape = 0
    # print "Memory usage of the acoustic embedding", acoustic_embedding.nbytes

    return lexData, labels, spkrData, acData, acoustic_embedding, ac_shape, lexData_e, lexData_k




def batch_size_modulo(batch_size, train, ac_train, labels, speakers, acoustic=False):
    data_set_size = len(train)

    # lacking_samples = batch_size - (data_set_size % batch_size)

    needed_samples = batch_size * int((data_set_size / batch_size))

    # for i in range(0, lacking_samples):
    #     idx = random.choice(range(1, len(train)))
    #     train.append(train[idx])
    #     labels.append(labels[idx])
    #     speakers.append(speakers[idx])
    #     if acoustic:
    #         ac_train.append(ac_train[idx])

    return train[:needed_samples], ac_train[:needed_samples], labels[:needed_samples], speakers[:needed_samples], needed_samples

def convert_to_one_hot(array, num_classes):
    b = np.zeros((len(array), num_classes))
    b[np.arange(len(array)), array-1] = 1

    return b


def load_lexical_data(corpus, batch_size, filter_heights=[3, 4, 5], num_filters = 100, context=1, data_file="/home/users0/vallejga/Desktop/gisela/thesis/LexicalData/w2v_ordered_lexicalData_with_speaker.p"):

    f = data_file

    x = cPickle.load(open(f, "rb"))
    revs, W, W2, word_idx_map, vocab, actDict, sent_len = x[0], x[1], x[2], x[3], x[4], x[5], x[6]

    num_classes = len(actDict) + 1 # if corpus == "swbd" else 6

    # These following lines pad the sentences with 4 zeros at the begining and at the end to avoid problems in the borders
    filter_h = 5
    pad = filter_h - 1
    padded_length = (context*sent_len)+(2*pad)
    lexical_hyperparams = (W, batch_size, padded_length, filter_heights, num_filters, num_classes)
    return revs, word_idx_map, sent_len, actDict, lexical_hyperparams
