import tensorflow as tf
import numpy as np


class TextCNN(object):
    """
    A CNN for text classification.
    Uses an embedding layer, followed by a convolutional, max-pooling and softmax layer.
    """
    def __init__(
            self, sequence_length, num_classes, vocab_size, embedding_size, filter_sizes, num_filters,
            context, crf, speaker, seed, l2_reg_lambda=0.0, batch_size=150):

        # Placeholders for input, speaker, output and dropout
        self.input_x = tf.placeholder(tf.int32, [batch_size, sequence_length], name="input_x")
        self.input_speaker = tf.placeholder(tf.float32, [batch_size, context], name="input_speaker")
        # Using only cnn we need the labels as a one-hot-vector
        if crf:
            self.input_y = tf.placeholder(tf.int32, [batch_size, context], name="input_y")
        else:
            self.input_y = tf.placeholder(tf.int32, [batch_size, num_classes], name="input_y")
        self.dropout_keep_prob = tf.placeholder(tf.float32, name="dropout_keep_prob")

        # Keeping track of l2 regularization loss (optional)
        l2_loss = tf.constant(0.0)
        # Keep a seed
        tf.set_random_seed(seed)

        # Embedding layer
        with tf.name_scope("embedding"):
            self.W = tf.Variable(
                tf.random_uniform([vocab_size, embedding_size], -1.0, 1.0),
                name="W",
                trainable=True)
            self.embedded_chars = tf.nn.embedding_lookup(self.W, self.input_x)
            self.embedded_chars_expanded = tf.expand_dims(self.embedded_chars, -1)

        if speaker:
            with tf.name_scope("speaker"):
                self.speaker_feature = tf.reshape(self.input_speaker,
                                                  [batch_size,
                                                   context,
                                                   1,
                                                   1],
                                                  name="speaker_feature")

        # concat embeddings with speaker
        # self.emb_speaker = tf.concat([self.embedded_chars_expanded, self.speaker_feature], 2)
        # new_emb_size = int(self.emb_speaker.get_shape()[2])

        # Create a convolution + maxpool layer for each filter size
        pooled_outputs = []
        for i, filter_size in enumerate(filter_sizes):
            with tf.name_scope("conv-maxpool-%s" % filter_size):
                # Convolution Layer
                # filter_shape = [filter_size, new_emb_size, 1, num_filters]
                filter_shape = [filter_size, embedding_size, 1, num_filters]
                W = tf.Variable(tf.truncated_normal(filter_shape, stddev=0.1), name="W")
                b = tf.Variable(tf.constant(0.1, shape=[num_filters]), name="b")
                conv = tf.nn.conv2d(
                    self.embedded_chars_expanded,
                    # self.emb_speaker,
                    W,
                    strides=[1, 1, 1, 1],
                    padding="VALID",
                    name="conv" + str(i))
                # Apply nonlinearity
                h = tf.nn.relu(tf.nn.bias_add(conv, b), name="relu")
                # Maxpooling over the outputs
                # ToDo: Check which pooling layer to use
                # pooled = tf.layers.max_pooling2d(h, ((sequence_length - filter_size + 1)/context, 1))
                if crf:
                    pooled = tf.nn.max_pool(h,
                                            ksize=[1, (sequence_length - filter_size + 1)/context, 1, 1],
                                            strides=[1, (sequence_length - filter_size + 1)/context, 1, 1],
                                            padding='VALID',
                                            name="pool")
                else:
                    pooled = tf.nn.max_pool(h,
                                            ksize=[1, sequence_length - filter_size + 1, 1, 1],
                                            strides=[1, 1, 1, 1],
                                            padding='VALID',
                                            name="pool")

                pooled_outputs.append(pooled)


       # Combine all the pooled features
        num_filters_total = num_filters * len(filter_sizes)
        self.h_pool = tf.concat(pooled_outputs, 3)
        if speaker:
            self.h_pool_speaker = tf.concat([self.h_pool, self.speaker_feature], 3)
            self.h_pool_flat = tf.reshape(self.h_pool_speaker, [-1, num_filters_total+1])
        else:
            self.h_pool_flat = tf.reshape(self.h_pool, [-1, num_filters_total])


        # Add dropout
        with tf.name_scope("dropout"):
            self.h_drop = tf.nn.dropout(self.h_pool_flat, self.dropout_keep_prob)


        # Final (unnormalized) scores and predictions
        with tf.name_scope("output"):
            if speaker:
                W = tf.get_variable(
                    "W",
                    shape=[num_filters_total+1, num_classes],
                    initializer=tf.contrib.layers.xavier_initializer(),
                    dtype=tf.float32)
            else:
                W = tf.get_variable(
                    "W",
                    shape=[num_filters_total, num_classes],
                    initializer=tf.contrib.layers.xavier_initializer(),
                    dtype=tf.float32)
            b = tf.Variable(tf.constant(0.0, shape=[num_classes]), name="b", dtype=tf.float32)
            l2_loss += tf.nn.l2_loss(W)
            l2_loss += tf.nn.l2_loss(b)
            self.scores = tf.nn.xw_plus_b(self.h_drop, W, b, name="scores")
            self.predictions = tf.argmax(self.scores, 1, name="predictions")

        if crf:
            with tf.name_scope("add_crf"):
                context_lengths = np.full(batch_size, context, dtype=np.int32)
                self.context_lengths_t = tf.constant(context_lengths, name="context_lengths_t")
                self.unary_scores = tf.reshape(self.scores, [batch_size, context, num_classes], name="unary_scores")
                self.log_likelihood, self.transition_params = tf.contrib.crf.crf_log_likelihood(
                    self.unary_scores, self.input_y, self.context_lengths_t)

                self.loss = tf.reduce_mean(-self.log_likelihood) + l2_reg_lambda * l2_loss
        else:
            with tf.name_scope("softmax"):
                losses = tf.nn.softmax_cross_entropy_with_logits(logits=self.scores, labels=self.input_y)
                self.loss = tf.reduce_mean(losses) + l2_reg_lambda * l2_loss

            # Accuracy
            with tf.name_scope("accuracy"):
                self.correct_predictions = tf.equal(self.predictions, tf.argmax(self.input_y, 1))
                self.accuracy = tf.reduce_mean(tf.cast(self.correct_predictions, "float"), name="accuracy")
